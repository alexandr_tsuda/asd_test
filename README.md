To run project execute command "**mvn clean install tomcat7:run**"

#API calls summary:
*1. /stores - POST
```javascript
{
    "name": String, *required
    "address": String *required
}
```

*2. /products - POST
```javascript
{
    "name": String, *required
    "description": String *required
}
```

*3. /stores/{id}/products - POST

```javascript
[{
    "name": String, *required
    "description": String, *required
    "price": double *required
}]
```

*4. /sellers -POST

```javascript
{
    "firstName": String, *required
    "lastName": String *required
}
```

*5. /store/{id}/sellers - POST

```javascript
[{
    "firstName": String, *required
    "lastName": String *required
}]
```

AND OTHER METHODS SPECIFIED BY REST SPECIFICATION