package com.asd.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Seller extends AbstractEntity {

    private String firstName;
    private String lastName;
    @JsonIgnore
    private Long storeId;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Long getStoreId() {
        return storeId;
    }

    public void setStoreId(Long storeId) {
        this.storeId = storeId;
    }
}
