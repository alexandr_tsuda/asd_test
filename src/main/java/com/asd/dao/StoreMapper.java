package com.asd.dao;

import com.asd.model.Store;

public interface StoreMapper extends CRUDMapper {

    Store loadBySeller(Long id);

}
