package com.asd.dao;

import com.asd.model.AbstractEntity;

public interface CRUDMapper {

    void create(AbstractEntity entity);

    AbstractEntity loadById(Long id);

    void update(AbstractEntity entity) ;

    void delete(AbstractEntity product);
}
