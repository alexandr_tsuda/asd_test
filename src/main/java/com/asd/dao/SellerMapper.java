package com.asd.dao;

import com.asd.model.Seller;

import java.util.List;

public interface SellerMapper extends CRUDMapper {

    List<Seller> loadByStore(Long id);
}
