package com.asd.dao;

import com.asd.model.Product;
import com.asd.model.Store;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ProductMapper extends CRUDMapper {

    void attachProductToStore(@Param("product") Product product, @Param("store") Store store);

    List<Product> loadProductsByStore(Long id);

    void detachProductFromStore(@Param("product") Product product, @Param("store") Store store);
}
