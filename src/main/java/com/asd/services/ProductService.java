package com.asd.services;

import com.asd.dao.ProductMapper;
import com.asd.model.Product;
import com.asd.model.Store;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ProductService {

    @Autowired
    ProductMapper productMapper;

    @Transactional(rollbackFor = Exception.class)
    public Product save(Product product) {
        productMapper.create(product);
        return product;
    }

    @Transactional(rollbackFor = Exception.class)
    public Product loadById(Long id) {
        return (Product) productMapper.loadById(id);
    }

    @Transactional(rollbackFor = Exception.class)
    public Product update(Product product) {
        productMapper.update(product);
        return product;
    }

    @Transactional(rollbackFor = Exception.class)
    public void delete(Product product) {
        productMapper.delete(product);
    }

    @Transactional(rollbackFor = Exception.class)
    public void attachProductToStore(Product product, Store store) {
        productMapper.attachProductToStore(product, store);
    }

    @Transactional(rollbackFor = Exception.class)
    public List<Product> loadProductsByStore(Long storeId) {
        return productMapper.loadProductsByStore(storeId);
    }

    public void detachProductFromStore(Product product, Store store) {
        productMapper.detachProductFromStore(product, store);
    }
}
