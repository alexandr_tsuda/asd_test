package com.asd.services;

import com.asd.dao.SellerMapper;
import com.asd.model.Seller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class SellerService {

    @Autowired
    SellerMapper sellerMapper;

    @Transactional(rollbackFor = Exception.class)
    public Seller save(Seller seller) {
        sellerMapper.create(seller);
        return seller;
    }

    @Transactional(rollbackFor = Exception.class)
    public Seller loadById(Long id) {
        return (Seller) sellerMapper.loadById(id);
    }

    @Transactional(rollbackFor = Exception.class)
    public Seller update(Seller seller) {
        sellerMapper.update(seller);
        return seller;
    }

    @Transactional(rollbackFor = Exception.class)
    public void delete(Seller seller) {
        sellerMapper.delete(seller);
    }

    public List<Seller> loadByStore(Long storeId) {
        return sellerMapper.loadByStore(storeId);
    }
}
