package com.asd.services;

import com.asd.dao.StoreMapper;
import com.asd.model.Product;
import com.asd.model.Seller;
import com.asd.model.Store;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class StoreService {

    @Autowired
    private StoreMapper storeMapper;

    @Autowired
    private ProductService productService;

    @Autowired
    private SellerService sellerService;

    @Transactional(rollbackFor = Exception.class)
    public Store save(Store store) {
        storeMapper.create(store);
        return store;
    }

    @Transactional(rollbackFor = Exception.class)
    public Store loadById(Long id) {
        return (Store) storeMapper.loadById(id);
    }

    @Transactional(rollbackFor = Exception.class)
    public Store update(Store store) {
        storeMapper.update(store);
        return store;
    }

    @Transactional(rollbackFor = Exception.class)
    public void delete(Store store) {
        storeMapper.delete(store);
    }

    @Transactional(rollbackFor = Exception.class)
    public Store loadBySeller(Long sellerId) {
        return storeMapper.loadBySeller(sellerId);
    }

    public List<Product> attachProducts(List<Product> products, Store store) {
        for (Product product : products) {
            if (product.getId() == null || product.getId().equals(0L)) {
                product = productService.save(product);
            } else {
                product = productService.loadById(product.getId());
            }
            productService.attachProductToStore(product, store);
        }
        return products;
    }

    public List<Seller> attachSeller(List<Seller> sellers, Store store) {
        for(Seller seller : sellers) {
            seller.setStoreId(store.getId());
            if (seller.getId() == null || seller.getId().equals(0L)) {
                sellerService.save(seller);
            } else {
                sellerService.update(seller);
            }
        }
        return sellers;
    }
}
