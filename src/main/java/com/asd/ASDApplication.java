package com.asd;

import org.glassfish.jersey.server.ResourceConfig;

import javax.ws.rs.ApplicationPath;

@ApplicationPath("resource")
public class ASDApplication extends ResourceConfig {

    public ASDApplication() {
        packages("com.asd.controllers");
    }
}
