package com.asd.controllers;

import com.asd.model.Product;
import com.asd.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Component
@Path("/products")
public class ProductController {

    @Autowired
    ProductService productService;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response create(Product product) {
        productService.save(product);
        return Response.status(201).entity(product).build();
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response loadById(@PathParam("id") Long id) {
        Product product = productService.loadById(id);
        return Response.status(200).entity(product).build();
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response update(Product product, @PathParam("id") Long id) {
        product.setId(id);
        productService.update(product);
        return Response.status(200).entity(product).build();
    }

    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    public Response delete(Product product) {
        productService.delete(product);
        return Response.status(200).build();
    }
}
