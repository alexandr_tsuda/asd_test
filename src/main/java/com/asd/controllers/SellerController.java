package com.asd.controllers;

import com.asd.model.Seller;
import com.asd.model.Store;
import com.asd.services.SellerService;
import com.asd.services.StoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Component
@Path("/sellers")
public class SellerController {

    @Autowired
    private SellerService sellerService;

    @Autowired
    private StoreService storeService;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response save(Seller seller) {
        sellerService.save(seller);
        return Response.status(201).entity(seller).build();
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response loadById(@PathParam("id") Long id) {
        Seller seller = sellerService.loadById(id);
        return Response.status(200).entity(seller).build();
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response update(Seller seller, @PathParam("id") Long id) {
        seller.setId(id);
        sellerService.update(seller);
        return Response.status(200).entity(seller).build();
    }

    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    public Response delete(Seller seller) {
        return Response.status(200).build();
    }

    @GET
    @Path("{id}/store")
    public Response loadStoreBySeller(@PathParam("id") Long id) {
        Store store = storeService.loadBySeller(id);
        return Response.status(200).entity(store).build();
    }
}
