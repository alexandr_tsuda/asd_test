package com.asd.controllers;

import com.asd.model.Product;
import com.asd.model.Seller;
import com.asd.model.Store;
import com.asd.services.ProductService;
import com.asd.services.SellerService;
import com.asd.services.StoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Component
@Path("/stores")
public class StoreController {

    @Autowired
    private StoreService storeService;

    @Autowired
    private SellerService sellerService;

    @Autowired
    private ProductService productService;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response save(Store store) {
        storeService.save(store);
        return Response.status(201).entity(store).build();
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response loadById(@PathParam("id") Long id) {
        Store store = storeService.loadById(id);
        return Response.status(200).entity(store).build();
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response update(Store store, @PathParam("id") Long id) {
        store.setId(id);
        storeService.update(store);
        return Response.status(200).entity(store).build();
    }

    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    public Response delete(Store store) {
        storeService.delete(store);
        return Response.status(200).build();
    }

    @GET
    @Path("{id}/sellers")
    @Produces(MediaType.APPLICATION_JSON)
    public Response loadSellersByStore(@PathParam("id") Long id) {
        List<Seller> sellers = sellerService.loadByStore(id);
        return Response.status(200).entity(sellers).build();
    }

    @GET
    @Path("{id}/products")
    @Produces(MediaType.APPLICATION_JSON)
    public Response loadProductsByStore(@PathParam("id") Long id) {
        List<Product> products = productService.loadProductsByStore(id);
        return Response.status(200).entity(products).build();
    }

    @POST
    @Path("/{id}/products")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response attachProducts(List<Product> products, @PathParam("id") Long id) {
        Store store = storeService.loadById(id);
        products = storeService.attachProducts(products, store);
        return Response.status(200).entity(products).build();
    }

    @POST
    @Path("/{id}/sellers")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response attachSellers(List<Seller> sellers, @PathParam("id") Long id) {
        Store store = storeService.loadById(id);
        sellers = storeService.attachSeller(sellers, store);
        return Response.status(200).entity(sellers).build();
    }
}
