import com.asd.model.Seller;
import com.asd.model.Store;
import com.asd.services.SellerService;
import com.asd.services.StoreService;
import junit.framework.TestCase;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {com.asd.ApplicationConfiguration.class}, loader = AnnotationConfigContextLoader.class)
public class StoreSellerRelationTest extends TestCase {

    private static final String STORE_NAME = "Store name";
    private static final String STORE_ADDRESS = "Store address";

    private static final Store STORE = new Store(){
        {
            setName(STORE_NAME);
            setAddress(STORE_ADDRESS);
        }
    };

    private static final String SELLER_FIRST_NAME = "FirstName";
    private static final String SELLER_LAST_NAME = "LastName";

    private static Seller SELLER = new Seller(){
        {
            setFirstName(SELLER_FIRST_NAME);
            setLastName(SELLER_LAST_NAME);
        }
    };

    private Long storeId;
    private Long sellerId;

    @Autowired
    SellerService sellerService;

    @Autowired
    StoreService storeService;

    @Before
    public void setUp() {
        storeId = storeService.save(STORE).getId();
        SELLER.setStoreId(storeId);
        sellerId = sellerService.save(SELLER).getId();
    }

    @Test
    public void loadStoreBySeller() {
        Store store = storeService.loadBySeller(sellerId);
        Assert.assertEquals(storeId, store.getId());
    }

    @After
    public void shutDown() {
        sellerService.delete(SELLER);
        storeService.delete(STORE);
    }
}
