import com.asd.model.Store;
import com.asd.services.StoreService;
import junit.framework.TestCase;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {com.asd.ApplicationConfiguration.class}, loader = AnnotationConfigContextLoader.class)
public class StoreMapperTest extends TestCase {

    private static final String STORE_NAME = "Name";
    private static final String STORE_ADDRESS = "Address";
    private static final String UPDATED_STORE_NAME = "Updated store name";
    private static final String UPDATED_STORE_ADDRESS = "Updated store address";

    private static final Store STORE = new Store(){
        {
            setName(STORE_NAME);
            setAddress(STORE_ADDRESS);
        }
    };

    private Long storeId;

    @Autowired
    StoreService storeService;

    @Before
    public void createStore() {
        Store store = storeService.save(STORE);
        storeId = store.getId();
        Assert.assertEquals(STORE_NAME, store.getName());
        Assert.assertEquals(STORE_ADDRESS, store.getAddress());
    }

    public Store loadById() {
        Store store = storeService.loadById(storeId);
        Assert.assertEquals(STORE_NAME, store.getName());
        Assert.assertEquals(STORE_ADDRESS, store.getAddress());
        Assert.assertEquals(storeId, store.getId());
        return store;
    }

    @Test
    public void update() {
        Store store = loadById();
        store.setName(UPDATED_STORE_NAME);
        store.setAddress(UPDATED_STORE_ADDRESS);
        storeService.update(store);
        store = storeService.loadById(storeId);
        Assert.assertEquals(UPDATED_STORE_NAME, store.getName());
        Assert.assertEquals(UPDATED_STORE_ADDRESS, store.getAddress());
        Assert.assertEquals(storeId, store.getId());
    }

    @After
    public void delete() {
        storeService.delete(STORE);
        Store store = storeService.loadById(storeId);
        Assert.assertEquals(store, null);
    }
}
