import com.asd.model.Seller;
import com.asd.services.SellerService;
import junit.framework.TestCase;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {com.asd.ApplicationConfiguration.class}, loader = AnnotationConfigContextLoader.class)
public class SellerMapperTest extends TestCase {

    private static final String SELLER_FIRST_NAME = "FirstName";
    private static final String SELLER_LAST_NAME = "LastName";
    private static final String UPDATED_FIRST_NAME = "updatedFirstName";
    private static final String UPDATED_LAST_NAME = "updatedLastName";

    private static final Seller SELLER = new Seller(){
        {
            setFirstName(SELLER_FIRST_NAME);
            setLastName(SELLER_LAST_NAME);
            setStoreId(null);
        }
    };

    private Long sellerId;

    @Autowired
    private SellerService sellerService;

    @Before
    public void create() {
        Seller seller = sellerService.save(SELLER);
        sellerId = seller.getId();
        Assert.assertEquals(SELLER_FIRST_NAME, seller.getFirstName());
        Assert.assertEquals(SELLER_LAST_NAME, seller.getLastName());
    }

    public Seller loadById() {
        Seller seller = sellerService.loadById(sellerId);
        Assert.assertEquals(sellerId, seller.getId());
        Assert.assertEquals(SELLER_FIRST_NAME, seller.getFirstName());
        Assert.assertEquals(SELLER_LAST_NAME, seller.getLastName());
        return seller;
    }

    @Test
    public void update() {
        Seller seller = loadById();
        seller.setFirstName(UPDATED_FIRST_NAME);
        seller.setLastName(UPDATED_LAST_NAME);
        sellerService.update(seller);
        seller = sellerService.loadById(sellerId);
        Assert.assertEquals(sellerId, seller.getId());
        Assert.assertEquals(UPDATED_FIRST_NAME, seller.getFirstName());
        Assert.assertEquals(UPDATED_LAST_NAME, seller.getLastName());
    }

    @After
    public void delete() {
        sellerService.delete(SELLER);
        Seller seller = sellerService.loadById(sellerId);
        Assert.assertEquals(seller, null);
    }
}
