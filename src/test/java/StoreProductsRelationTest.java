import com.asd.model.Product;
import com.asd.model.Store;
import com.asd.services.ProductService;
import com.asd.services.StoreService;
import junit.framework.TestCase;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {com.asd.ApplicationConfiguration.class}, loader = AnnotationConfigContextLoader.class)
public class StoreProductsRelationTest extends TestCase {

    private static final String STORE_NAME = "Store name";
    private static final String STORE_ADDRESS = "Store address";

    private static final String FIRST_PRODUCT_NAME = "First";
    private static final String FIRST_PRODUCT_DESCRIPTION = "First description";
    private static final BigDecimal FIRST_PRODUCT_PRICE = new BigDecimal(10d);

    private static final String SECOND_PRODUCT_NAME = "Second";
    private static final String SECOND_PRODUCT_DESCRIPTION = "Second description";
    private static final BigDecimal SECOND_PRODUCT_PRICE = new BigDecimal(20d);

    private static final String SINGLE_PRODUCT_NAME = "Single";
    private static final String SINGLE_PRODUCT_DESCRIPTION = "Single description";

    private static final Product FIRST_PRODUCT = new Product(){
        {
            setName(FIRST_PRODUCT_NAME);
            setDescription(FIRST_PRODUCT_DESCRIPTION);
            setPrice(FIRST_PRODUCT_PRICE);
        }
    };

    private static final Product SECOND_PRODUCT = new Product(){
        {
            setName(SECOND_PRODUCT_NAME);
            setDescription(SECOND_PRODUCT_DESCRIPTION);
            setPrice(SECOND_PRODUCT_PRICE);
        }
    };

    private static final Product SINGLE_PRODUCT = new Product(){
        {
            setName(SINGLE_PRODUCT_NAME);
            setDescription(SINGLE_PRODUCT_DESCRIPTION);
        }
    };

    private static final Store STORE = new Store(){
        {
            setName(STORE_NAME);
            setAddress(STORE_ADDRESS);
        }
    };

    private Long firstProductId;
    private Long secondProductId;
    private Long singleProductId;
    private Long storeId;

    @Autowired
    StoreService storeService;

    @Autowired
    ProductService productService;

    @Before
    public void setUp() {
        storeId = storeService.save(STORE).getId();
        List<Product> products = new ArrayList<Product>(){{
            add(0, FIRST_PRODUCT);
            add(1, SECOND_PRODUCT);
        }};
        products = storeService.attachProducts(products, STORE);
        firstProductId = products.get(0).getId();
        secondProductId = products.get(1).getId();
        singleProductId = productService.save(SINGLE_PRODUCT).getId();
    }

    @Test
    public void loadProductsByStore() {
        List<Product> products = productService.loadProductsByStore(storeId);
        Assert.assertEquals(products.size(), 2);
        Assert.assertEquals(products.get(0).getId(), firstProductId);
        Assert.assertEquals(products.get(1).getId(), secondProductId);
    }

    @After
    public void shutdown() {
        productService.detachProductFromStore(FIRST_PRODUCT, STORE);
        productService.detachProductFromStore(SECOND_PRODUCT, STORE);
        storeService.delete(STORE);
        productService.delete(FIRST_PRODUCT);
        productService.delete(SECOND_PRODUCT);
        productService.delete(SINGLE_PRODUCT);
    }
}
