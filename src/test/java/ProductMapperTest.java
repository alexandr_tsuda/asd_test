import com.asd.model.Product;
import com.asd.services.ProductService;
import junit.framework.TestCase;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {com.asd.ApplicationConfiguration.class}, loader = AnnotationConfigContextLoader.class)
public class ProductMapperTest extends TestCase {

    private static final String PRODUCT_NAME = "Name";
    private static final String PRODUCT_DESCRIPTION = "Description";
    private static final String UPDATED_PRODUCT_NAME = "Updated name";
    private static final String UPDATED_PRODUCT_DESCRIPTION = "Updated description";

    private static final Product PRODUCT = new Product(){
        {
            setName(PRODUCT_NAME);
            setDescription(PRODUCT_DESCRIPTION);
        }
    };

    private Long productId;

    @Autowired
    ProductService productService;

    @Before
    public void create() {
        Product product = productService.save(PRODUCT);
        productId = product.getId();
        Assert.assertEquals(PRODUCT_NAME, product.getName());
        Assert.assertEquals(PRODUCT_DESCRIPTION, product.getDescription());
    }

    public Product loadById() {
        Product product = productService.loadById(productId);
        Assert.assertEquals(productId, product.getId());
        Assert.assertEquals(PRODUCT_NAME, product.getName());
        Assert.assertEquals(PRODUCT_DESCRIPTION, product.getDescription());
        return  product;
    }

    @Test
    public void update() {
        Product product = loadById();
        product.setName(UPDATED_PRODUCT_NAME);
        product.setDescription(UPDATED_PRODUCT_DESCRIPTION);
        productService.update(product);
        product = productService.loadById(productId);
        Assert.assertEquals(productId, product.getId());
        Assert.assertEquals(UPDATED_PRODUCT_NAME, product.getName());
        Assert.assertEquals(UPDATED_PRODUCT_DESCRIPTION, product.getDescription());
    }

    @After
    public void delete() {
        productService.delete(PRODUCT);
        Product product = productService.loadById(productId);
        Assert.assertEquals(product, null);
    }
}
